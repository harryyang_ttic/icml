%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% ICML 2015 EXAMPLE LATEX SUBMISSION FILE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use the following line _only_ if you're still using LaTeX 2.09.
%\documentstyle[icml2015,epsf,natbib]{article}
% If you rely on Latex2e packages, like most moden people use this:
\documentclass{article}

% use Times
\usepackage{times}
% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
%\usepackage{subfigure} 
%\usepackage{subcaption}
% For citations
\usepackage{natbib}
\usepackage{caption}
% For algorithms
%\usepackage{algorithm}
%\usepackage{algorithmic}
\usepackage{algorithm2e}
% As of 2011, we use the hyperref package to produce hyperlinks in the
% resulting PDF.  If this breaks your system, please commend out the
% following usepackage line and replace \usepackage{icml2015} with
% \usepackage[nohyperref]{icml2015} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Employ the following version of the ``usepackage'' statement for
% submitting the draft version of the paper for review.  This will set
% the note in the first column to ``Under review.  Do not distribute.''
\usepackage{icml2015} 
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[amsmath,thmmarks]{ntheorem}
\usepackage{graphicx}
\newtheorem{lem}{Lemma}
\usepackage{float}
\usepackage{bm}
\usepackage{subfigure}

% Employ this version of the ``usepackage'' statement after the paper has
% been accepted, when creating the final version.  This will set the
% note in the first column to ``Proceedings of the...''
%\usepackage[accepted]{icml2015}


% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Submission and Formatting Instructions for ICML 2015}

\begin{document} 

\twocolumn[
\icmltitle{Exact Hybrid Covariance Thresholding for Joint Graphical Lasso}

% It is OKAY to include author information, even for blind
% submissions: the style file will automatically remove it for you
% unless you've provided the [accepted] option to the icml2015
% package.


% You may provide any keywords that you 
% find helpful for describing your paper; these are used to populate 
% the "keywords" metadata in the PDF but will not be shown in the document

\vskip 0.3in
]

\begin{abstract} 
 This paper considers the problem of estimating multiple related Gaussian graphical models from a $p$-dimensional dataset consisting of different classes. Our work is based upon the formulation of this problem as group graphical lasso. This paper proposes a novel hybrid covariance thresholding algorithm that can effectively identify zero entries in the precision matrices and split a large joint graphical lasso problem into small subproblems. Our hybrid covariance thresholding method is superior to existing uniform thresholding methods in that our method can split the precision matrix of each individual class using different partition schemes and thus split group graphical lasso into much smaller subproblems, each of which can be solved very fast. In addition, this paper establishes necessary and sufficient conditions for our hybrid covariance thresholding algorithm. The superior performance of our thresholding method is thoroughly analyzed and illustrated by a few experiments on simulated data and real gene expression data.
\end{abstract} 

\section{Introduction}

Graphs have been widely used to describe the relationship between variables (or features). Estimating an undirected graphical model from a dataset has been extensively studied.  When the dataset has a Gaussian distribution, the problem is equivalent to estimating a precision matrix from the empirical (or sample) covariance matrix. In many real-world applications, the precision matrix is sparse. This problem can be formulated as graphical lasso [1, 2] and many algorithms [3-7] have been proposed to solve it. To take advantage of the sparsity of the precision matrix, some screening methods are developed to detect zero entries in the matrix and then split the matrix into smaller submatrices, which can significantly speed up the process of estimating the entire precision matrix [6, 8].

Recently, there are a few studies on how to jointly estimate multiple related graphical models from a dataset with a few distinct class labels [9-20]. The underlying reason for joint estimation is that the graphs of these classes are similar to some degree, so it can increase statistical power and estimation accuracy by aggregating data of different classes. This joint graph estimation problem can be formulated as joint graphical lasso that makes use of similarity of the underlying graphs. In addition, [10] used a non-convex hierarchical penalty to promote similar patterns among multiple graphical models; [9] introduced popular group and fused graphical lasso; and [17, 18] proposed efficient algorithms to solve fused graphical lasso. To model gene networks, [15] proposed a node-based penalty to promote hub structure in a graph.

Existing algorithms for solving joint graphical lasso do not scale well with respect to the number of classes, denoted as $K$, and the number of variables, denoted as p. Similar to thresholding methods for graphical lasso, a couple of thresholding (also called screening) methods [17, 18] are developed to split a large joint graphical lasso problem into subproblems [9]. Nevertheless, these screening algorithms all use uniform thresholding to decompose the precision matrices of distinct classes in exactly the same way. As such, it may not split the precision matrices into small enough submatrices especially when there are a large number of classes and the precision matrices have different sparsity patterns. Therefore, the speedup effect of screening may not be very significant.

In contrast to uniform thresholding, this paper presents a novel hybrid (or non-uniform) thresholding approach that can divide the precision matrix for each individual class into smaller submatrices without requiring that the resultant partition schemes to be exactly the same across all the classes. Using this method, we can split a large joint graphical lasso problem into much smaller subproblems. Then we employ the popular ADMM (Alternating Direction Method of Multipliers [21, 22]) method to solve joint graphical lasso based upon this hybrid partition scheme. Experiments show that our method can solve group graphical lasso much more efficiently than uniform thresholding.

We demonstrate our hybrid thresholding approach through group graphical lasso. The principle can be generalized to other joint penalties. Due to space limit, the proofs of all the theorems of the paper are presented in supplementary material.

\section{Notation and Definition}

In this paper, we use a script letter, like $\mathcal{H}$, to denote a set or a set partition. When $\mathcal{H}$ is a set, we use $\mathcal{H}_i$ to denote the $i^{\text{th}}$ element. Similarly we use a bold letter, like $\bm{H}$ to denote a graph, a vector or a matrix. When $\bm{H}$ is a matrix we use $\bm{H}_{i,j}$ to denote its $(i,j)^{th}$ entry. We use $\{ \mathcal{H}^{(1)},\mathcal{H}^{(2)}, \ldots,\mathcal{H}^{(N)}\}$ and $\{\bm{H}^{(1)},\bm{H}^{(2)}\,\ldots,\bm{H}^{(N)}\}$ to denote $N$ objects of same category.

Let $\{\bm{X}^{(1)},\bm{X}^{(2)},\ldots,\bm{X}^{(K)} \}$ denote a sample dataset of $K$ classes and the data in $\bm{X}^{(k)}\ (1 \leq k \leq K)$ are independently and identically drawn from a $p$-dimension normal distribution $N(\bm{\mu} ^{(k)}, \bm{\Sigma}^{(k)})$. Let $\bm{S}^{(k)}$ and $\hat{\bm{\Theta}}^{(k)}$ denote the empirical covariance and (optimal) precision matrices of class $k$, respectively. By ``optimal'' we mean the precision matrices are obtained by exactly solving joint graphical lasso. Let a binary matrix $\bm{E}^{(k)}$ denote the sparsity pattern of $\hat{\bm{\Theta}}^{(k)}$, i.e., for any $i,j (1\leq i,j \leq p), \bm{E}_{(i,j)}^{(k)}=1$ if and only if $\hat{\bm{\Theta}}Θ_{i,j}^{(k)} \neq 0$.


\textbf{Set partition.} A set $\mathcal{H}$ is a partition of a set $\mathcal{C}$ when the following conditions are satisfied: 1) any element in $\mathcal{H}$ is a subset of $\mathcal{C}$; 2) the union of all the elements in $\mathcal{H}$ is equal to $\mathcal{C}$; and 3) any two elements in $\mathcal{H}$ are disjoint. Given two partitions $\mathcal{H}$ and $\mathcal{F}$ of a set $\mathcal{C}$, we say that $\mathcal{H}$ is finer than $\mathcal{F}$ (or $\mathcal{H}$ is a refinement of $\mathcal{F}$), denoted as $\mathcal{H} \preceq \mathcal{F}$, if every element in $\mathcal{H}$ is a subset of some element in $\mathcal{F}$. If $\mathcal{H} \preceq \mathcal{F}$ and $\mathcal{H} \neq \mathcal{F}$, we say that $\mathcal{H}$ is strictly finer than $\mathcal{F}$ (or $\mathcal{H}$ is a strict refinement of $\mathcal{F}$), denoted as $\mathcal{H} \prec \mathcal{F}$.

Let $\bm{\Theta}$ denote a matrix describing the pairwise relationship of elements in a set $\mathcal{C}$, where $\bm{\Theta}_{i,j}$ corresponds to two elements $\mathcal{C}_{i}$ and $\mathcal{C}_{j}$. Given a partition $\mathcal{H}$ of $\mathcal{C}$, we define $\bm{\Theta}_{\mathcal{H}_{k}}$as a $|\mathcal{H}_{k}| \times |\mathcal{H}_{k}|$ submatrix of $\bm{\Theta}$ where $\mathcal{H}_{k}$ is an element of $\mathcal{H}$ and
$(\bm{\Theta}_{\mathcal{H}_{k}})_{i,j} \cong   \bm{\Theta}_{(\mathcal{H}_{k})_{i}(\mathcal{H}_{k})_{j}}$ for any suitable ($i,j$).


\textbf{Graph-based partition.}	

%Let $\{ \mathcal{Y}^{(1)}, \mathcal{Y}^{(2)}, \dots, \mathcal{Y}^{(K)} \}$ denote a sample %dataset of $K$ classes and the data in $\mathcal{Y}^{(k)}\ ( 1 \leq k \leq K) $  are %independently and identically drawn from a $p$-dimension normal distribution %$\mathcal{N}(\mu^{(k)},\Sigma^{(k)})$. Let $\mathcal{S}^{(k)}$ and $\bm{\Theta}^{(k)}$ denote the %empirical covariance and (optimal) precision matrices of class $k$, respectively. By \lq\lq %optimal\rq\rq we mean the precision matrix is obtained by solving joint graphical lasso exactly. %Let a binary matrix $\textbf{E}^{(k)}$ denote the sparsity pattern of $\hat{\bm{\Theta}}^{(k)}$, i.e., %for any $i,j \ (1\leq i,j \leq p)$, $\textbf{E}_{i,j}^{(k)}=1$ if and only if %$\hat{\bm{\Theta}}^{(k)}_{i,j} \neq 0$.
		
Let $\mathcal{V}=\{1,2,\ldots,p\}$ denote the variable (or feature) set of the dataset. Let graph $ \bm{G}^{(k)}=(\mathcal{V} ,\bm{E}^{(k)})$ denote the $k^{\text{th}}$ estimated concentration graph $1 \leq k \leq K$. This graph defines a partition $\boxplus^{(k)}$  of $\mathcal{V}$, where an element in $\boxplus^{(k)}$ corresponds to a connected component in $\bm{G}^{(k)}$. The matrix $\hat{\bm{\Theta}}^{(k)}$ can be divided into disjoint submatrices based upon $\boxplus^{(k)}$. Let $\bm{E}$ denote the mix of $\bm{E}^{(1) },\bm{E}^{(2)}, \ldots, \bm{E}^{(K)}$, i.e., one entry $\bm{E}_{i,j}$ is equal to 1 if there exists at least one $k\ (1\leq k \leq K )$ such that $\bm{E}_{i,j}^{(k)}$ is equal to 1. We can construct a partition $\boxplus$ of $\mathcal{V}$ from graph $\bm{G}=\{\mathcal{V},\bm{E}\}$, where an element in $\boxplus$ corresponds to a connected component in $\bm{G}$. Obviously, $\boxplus^{(k)} \preccurlyeq \boxplus$ holds since $\bm{E}^{(k)}$ is a subset of $\bm{E}$. This implies that for any $k$, the matrix $\hat{\bm{\Theta}}^{(k)}$ can be divided into disjoint submatrices based upon $\boxplus$.


\textbf{Feasible partition.} A partition $\mathcal{H}$ of $\mathcal{V}$ is feasible for class $k$ or graph $\bm{G}^{(k)}$ if $\boxplus^{(k)} \preccurlyeq \mathcal{H}$. This implies that 1) $\mathcal{H}$ can be obtained by merging some elements in $\boxplus^{(k)}$; 2) each element in $\mathcal{H}$ corresponds to a union of some connected components in graph $\bm{G}^{(k)}$; and 3) we can divide the precision matrix $\hat{\bm{\Theta}}^{(k)}$ into independent submatrices according to $\mathcal{H}$ and then separately estimate the submatrices without losing accuracy. $\mathcal{H}$ is uniformly feasible if for all $k\ (1 \leq k \leq K)$, $\boxplus^{(k)} \preccurlyeq \mathcal{H}$ holds.


Let $\mathcal{H}^{(1)},\mathcal{H}^{(2)},\ldots,\mathcal{H}^{(K)}$ denote $K$ partitions of the variable set $V$. If for each $k\ (1 \leq k \leq K),\ \boxplus^{(k)} \preccurlyeq \mathcal{H}^{(k)}$ holds, we say $\{\mathcal{H}^{(1)}, \mathcal{H}^{(2) },\ldots, \mathcal{H}^{(K)} \}$ is a feasible partition of $\mathcal{V}$ for the $K$ classes or graphs. When at least two of the $K$ partitions are not same, we say $\{ \mathcal{H}^{(1)}, \mathcal{H}^{(2)},\ldots, \mathcal{H}^{(K)} \}$ is a class-specific or non-uniform partition. Otherwise, $\{ \mathcal{H}^{(1)}, \mathcal{H}^{(2)},\ldots, \mathcal{H}^{(K)} \}$ is a class-independent or uniform partition and abbreviated as $H$. That is, $\mathcal{H}$ is uniformly feasible if for all $k$ $(1\leq k \leq K)$, $\boxplus^{(k)} \preccurlyeq \mathcal{H}$ holds.
Obviously, $\{ \boxplus^{(1)}, \boxplus^{(2)}, \ldots,\boxplus^{(K)} \}$ is finer than any non-uniform feasible partition of the $K$ classes. Based upon the above definitions, we have the following theorem, which is proved in supplementary material.


\newtheorem{lemma}{Theorem}
\begin{lemma}
 For any uniformly feasible partition $\mathcal{H}$ of the variable set $\mathcal{V}$, we have $\boxplus \preccurlyeq  \mathcal{H}$. That is, $\mathcal{H}$ is feasible for graph $\textbf{G}$ and $\boxplus$ is the finest uniform feasible partition.
\end{lemma}


\section{Joint Graphical Lasso}


To learn the underlying graph structure of multiple classes simultaneously, some penalty functions are used to promote similar structural patterns among different classes, including [4, 9-11, 14, 15, 17, 18, 23]. A typical joint graphical lasso is formulated as the following optimization problem:

\begin{equation}
\min (\sum_{k=1}^{K} L({\bm{\Theta}}^{(k)}) + P(\bm{\Theta}) )
\end{equation}


Where $\bm{\Theta}^{(k)} \succ 0$ is the precision matrix $(k=1, \dots ,K)$ and $\bm{\Theta}$ represents the set of $\bm{\Theta}^{(k)}$. The negative log-likelihood $L(\bm{\Theta}^{(k)})$ and the regularization $P(\bm{\Theta})$  are defined as follows.
\begin{equation}
L(\bm{\Theta}^{(k)}) = -\log \det (\bm{\Theta}^{(k)}) + tr(\mathcal{S}^{(k)}{\bm{\Theta}}^{(k)})
\end{equation}

\begin{equation}
P(\bm{\Theta}) = \lambda_{1}\sum_{k=1}^{K}\| \bm{\Theta}^{(k)}\|_{1} + \lambda_{2}J(\bm{\Theta})
\end{equation}


 Here $\lambda_{1}>0$ and $\lambda_{2}>0$ and $J(\bm{\Theta})$ is some penalty function used to encourage similarity (of the structural patterns) among the $K$ classes. In this paper, we focus on group graphical lasso. That is,


\begin{eqnarray}
J(\bm{\Theta}) = 2\sum_{1 \leq i<j\leq p} \sqrt{\sum_{k=1}^{K}(\bm{\Theta}_{i,j}^{(k)})^{2}}
\end{eqnarray}



\section{Uniform Thresholding}

Screening methods, which identify zero entries in a precision matrix before directly solving the optimization problem defined in Eq. (1), are widely used to accelerate solving graphical lasso. In particular, a screening method divides the variable set into some disjoint groups such that when two variables (or features) are not in the same group, their corresponding entry in the precision matrix is guaranteed to be 0. Using this method, the precision matrix can be split into some submatrices, each corresponding to one distinct group. To achieve the best computational efficiency, we shall divide the variable set into as small groups as possible subject to the constraint that two related variables shall be in the same group. Meanwhile, [9] described a screening method for group graphical lasso. This method uses a single thresholding criterion (i.e., uniform thresholding) for all the K classes, i.e., employs a uniformly feasible partition of the variable set across all the K classes. Existing methods such as those described in [9, 17, 18] for fused graphical lasso and that in [19] for node-based learning all employ uniform thresholding.

Uniform thresholding may not be able to divide the variable set into the finest feasible partition for each individual class when the K underlying concentration graphs are not exactly the same. For example, Figure 1(a) and (c) show two concentration graphs of two different classes. These two graphs differ in variables 1 and 6 and each graph can be split into two connected components. However, the mixing graph in (b) has only one connected component, so it cannot be split further. According to \textbf{Theorem 1}, no uniform feasible partition can divide the variable set into two disjoint groups without losing accuracy. It is expected that when the number of classes and variables increases, uniform thresholding may perform even worse.


\begin{figure}[h]
\vspace{.3in}
\centering
%\begin{subfigure}[b]{0.17\textwidth}
\includegraphics[width=0.17\textwidth]{img/img1/Fig1a}
%\caption{}
%\end{subfigure}
%\centerline{(a)}
%\hspace{3ex}
%\begin{subfigure}[b]{0.09\textwidth}
\includegraphics[width=0.09\textwidth]{img/img1/Fig1b}
%\caption{}
%\end{subfigure}
%\centerline{(b)}
%\hspace{3ex}
%\begin{subfigure}[b]{0.17\textwidth}
\includegraphics[width=0.17\textwidth]{img/img1/Fig1c}
%\caption{}
%\end{subfigure}
%\centerline{(c)}
%\vspace{.3in}
\caption{Illustration of uniform thresholding impacted by minor structure difference between two classes. (a) and (c): the edge matrix and concentration graph for each of the two classes. (b): the concentration graph resulting from the mixing of two graphs in (a) and (c).}
\end{figure}


\section{Non-uniform Thresholding}

Non-uniform thresholding generates a non-uniform feasible partition by thresholding the $K$ empirical covariance matrices separately. In a non-uniform partition, two variables of the same group in one class may belong to different groups in another class. Figure 1 in supplementary material shows an example of non-uniform partition. In this example, all the matrix elements in white color are set to 0 by non-uniform thresholding. Except the white color, each of the other colors indicates one group. The $7^{\text{th}}$ and $9^{\text{th}}$ variables belong to the same group in the left matrix, but not in the right matrix. Similarly, the $3^{\text{rd}}$ and $4^{\text{th}}$ variables belong to the same group in the right matrix, but not in the left matrix.

We now present necessary and sufficient conditions for identifying a non-uniform feasible partition for group graphical lasso, with penalty defined in Eq (3) and (4).

Given a non-uniform partition $\{ \mathcal{P}^{(1)}, \mathcal{P}^{(2)}, \ldots, \mathcal{P}^{(K)} \}$ for the $K$ classes, let $F^{(k)}(i)$ denote the group which the variable $i$ belongs to in the $k^{\text{th}}$ class, i.e., $F^{(k)}(i) \Leftrightarrow i\in \text{P}_{t}^{(k)}$. We define pairwise relationship matrices $\textbf{I}^{(k)}\ (1 \leq k \leq K)$ as follows:

\begin{equation}
\begin{cases}
\textbf{I}_{i,j}^{(k)} = \textbf{I}_{j,i}^{(k)} = 0;\ \text{if} \ F^{(k)}(i) \neq F^{(k)}(j) \\
\textbf{I}_{i,j}^{(k)} = \textbf{I}_{j,i}^{(k)} = 1;\ \text{otherwise}
\end{cases}
\end{equation}


Also, we define $\bm{Z}^{(k)}(1 \leq k \leq K)$ as follows:
\begin{equation}
\bm{Z}_{i,j}^{(k)} = \bm{Z}_{j,i}^{(k)} = \lambda_{1} + \lambda_{2} \times \tau ((\sum_{t \neq k} |\hat{\bm{\Theta}}_{i,j}^{(t)}|) = 0)
\end{equation}
Here $\tau(b)$ is the indicator function.

The following two theorems state the necessary and sufficient conditions of a non-uniform feasible partition. See supplementary material for their proofs.

\begin{lemma}

 If $\{ \mathcal{P}^{(1)}, \mathcal{P}^{(2)}, \ldots, \mathcal{P}^{(K)} \} $ is a non-uniform feasible partition of the variable set $\mathcal{V}$, then for any pair $(i,j)\ (1 \leq i \neq j \leq p)$ the following conditions must be satisfied:
 \begin{equation}
 \begin{cases}
  \sum_{k=1}^{K}( |\mathcal{\textbf{S}}_{i,j}^{(k)}| - \lambda_{1})_{+}^{2}     \leq \lambda_{2}^{2}; \ \text{if}  \ \forall k \in 1,2, \ldots, K, \textbf{I}_{i,j}^{(k)} =0\\
  |\mathcal{\textbf{S}}_{i,j}^{(k)}| \leq \mathcal{\textbf{Z}}_{i,j}^{(k)}; \ \text{if} \ \textbf{I}_{i,j}^{(k)} =0 \ \text{and} \ \exists t \neq k, \textbf{I}_{i,j}^{(t)} = 1
 \end{cases}
 \end{equation}

Here, each $\mathcal{\textbf{S}}^{(k)}$ is a covariance matrix of the $k^{th}$ class and $x_{+} = \max(0, x)$.

\end{lemma}

\begin{lemma}

 If for any pair $(i,j)(1 \leq i \neq j \leq p)$ the following conditions hold, then $\{\mathcal{P}^{(1)}, \mathcal{P}^{(2)}, \ldots, \mathcal{P}^{(K)} \}$ is a non-uniform feasible partition of the variable set $\mathcal{V}$.
 \begin{equation}
 \begin{cases}
  \sum_{k=1}^{K}( |\mathcal{\textbf{S}}_{i,j}^{(k)}| - \lambda_{1})_{+}^{2}     \leq \lambda_{2}^{2};\   \text{if}  \ \forall k \in 1,2, \ldots, K, \textbf{I}_{i,j}^{(k)} =0		\\
  |\mathcal{\textbf{S}}_{i,j}^{(k)}| \leq \lambda_{1}; \ \text{if} \ \textbf{I}_{i,j}^{(k)} =0 \ \text{and} \ \exists t \neq k, \textbf{I}_{i,j}^{(t)} = 1
 \end{cases}
 \end{equation}

\end{lemma}
\begin{algorithm}[h]
\SetAlgoLined
\caption{Hybrid Covariance Screening Algorithm}
\KwIn{$\lambda_{1}$, $\lambda_{2}$, $K$, $p\times p$ covariance matrices $\{\bm{S}^{(1)}, \bm{S}^{(2)}, \ldots, \bm{S}^{(K)} \}$}
\KwOut{A non-uniform feasible partition of the variable set $\mathcal{V}$ }
\For{$k=1\ to\ K$}
{
 Initialize $\textbf{I}_{i,j}^{(k)} = \textbf{I}_{j,i}^{(k)} = 1$, $\forall 1 \leq i < j \leq p$\;
 Set $\textbf{I}_{i,j}^{(k)} = 0 $, if $|\bm{S}_{i,j}^{(k)}| \le \lambda_{1}$ and $i \neq j$\;
 Set $\textbf{I}_{i,j}^{(k)} = 0 $, if $\sum_{k=1}^{K}(|\bm{S}_{i,j}^{(k)}| - \lambda_{1})_{+}^{2} \le \lambda_{2}^{2}$ and $i \neq j$\;
}

\For{$k=1\ to\ K$}
{
 Construct a graph $\bm{G}^{(k)}$ for $\mathcal{V}$ from $\bm{I}^{(k)}$\;
 Find connected components of $G^{(k)}$\;
 \For{$\forall (i,j)\ in\ the\ same\ component\ of\ \bm{G}_{(k)}$}
 {
 	Set $\bm{I}_{i,j}^{k}=\bm{I}_{j,i}^{(k)}=1$\;
 }
}
\textbf{repeat}\\
Search for triple $(x,i,j)$ satisfying the following condition:\\
$\bm{I}_{i,j}^k=0$, $|S_{i,j}|^{(x)}>\lambda_1$ and $\exists s$, s.t. $\bm{I}_{i,j}^{(s)}=1$ \;
\If{$\exists (x,i,j)$ satisfies the condition above}
{
		merge the two components of $\bm{G}^{(x)}$ that
		containing variable $i$ and $j$ into new component;\\
		\For{$\forall (m,n)$ in this new component}
		{
			Set $\bm{I}_{m,n}^{(x)}=\bm{I}_{n,m}^{(x)}=1$;
		}
	}
\textbf{until} No such kind of triple.\\
\textbf{return} the connected components of each graph which define the non-uniform feasible solution\;
\end{algorithm}
\textbf{Algorithm 1} is a covariance thresholding algorithm that can identify a non-uniform feasible partition satisfying condition (8). We call \textbf{Algorithm 1} hybrid screening algorithm as it utilizes both class-specific thresholding (e.g. $|\mathcal{\textbf{S}}_{i,j}^{(k)}| \leq \lambda_{1}$ ) and global thresholding (e.g. $\sum_{k=1}^{K}( |\mathcal{\textbf{S}}_{i,j}^{(k)}| - \lambda_{1})_{+}^{2} \leq \lambda_{2}^{2}$ ) to identify a non-uniform partition. This hybrid screening algorithm can terminate within seconds on a typical Linux machine, tested on the synthetic data described in section 7 with $K=10$ and $p=10000$.

We can generate a uniform feasible partition using only the global thresholding, which is exactly what described in paper [9]. We can also generate a non-uniform feasible partition by using only the class-specific thresholding, but such a partition is not as good as using the hybrid algorithm. Let $\{ \mathcal{H}^{(1) },\mathcal{H}^{(2) }, \ldots,\mathcal{H}^{(K) } \}$ ,  $\{\mathcal{L}^{(1)}, \mathcal{L}^{(2)},\ldots,\mathcal{L}^{(K)} \}$  and $\mathcal{G}$ denote the partitions generated by hybrid, class-specific and global thresholding algorithms, respectively. It is obvious that $\mathcal{H}^{(k)} \preccurlyeq \mathcal{L}^{(k)}$ and $\mathcal{H}^{(k)} \preccurlyeq \mathcal{G}$ for $k=1,2,\dots,K$ since condition (8) is a combination of both global thresholding and class-specific thresholding.

Figure 2 in supplementary material shows a toy example comparing the three screening methods using a dataset of two classes and three variables. In this example, the class-specific or the global thresholding alone cannot divide the variable set into disjoint groups, but their combination can do so.

We have the following theorem regarding our hybrid thresholding algorithm, which will be proved in Supplemental File.

\begin{lemma}
 The hybrid screening algorithm yields the finest non-uniform feasible partition satisfying condition (8).
\end{lemma}

\section{Hybrid ADMM (Alternate Direction Methods of Multiplier)}
In this section, we describe how to apply ADMM (Alternating Direction Method of Multipliers [21, 22]) to solve joint graphical lasso based upon a non-uniform feasible partition of the variable set. According to [9], to solve Eq (1) subject to the given positive definite constraints using ADMM is equal to minimize the following scaled augmented Lagrangian form:

\begin{equation}
\begin{aligned}
\sum\limits_{k=1}^KL(\bm{\Theta}^{(k)})+\frac{\rho}{2}\sum_{k=1}^{K}\| \bm{\Theta}^{(k)} - \bm{Y}^{(k)} + \bm{U}^{(k)}\|_{F}^{2}+P(Y)
\end{aligned}
\end{equation}

 where $\bm{Y} = \{\bm{Y}^{(1)}, \bm{Y}^{(1)}, \ldots, \bm{Y}^{(K)} \}$ and $\bm{U} = \{ \bm{U}^{(1)}, \bm{U}^{(1)}, \ldots, \bm{U}^{(K)} \}$ are dual variables. We use the ADMM algorithm to solve Eq (9) iteratively, which updates the three variables $\bm{\Theta}$, $\bm{Y}$ and $\bm{U}$ alternatively. The most computational-insensitive step is to update $\bm{\Theta}$ given $\bm{Y}$ and $\bm{U}$, which requires eigen-decomposition of $K$ matrices. We can do this based upon a non-uniform feasible partition $\{ \mathcal{H}^{(1)}, \mathcal{H}^{(2)}, \ldots, \mathcal{H}^{(K)} \}$. For each $k$, updating $^{(k)}$ given $\bm{Y}^{(k)}$  and $\bm{U}^{(k)}$ for Eq (9) is equivalent to solving total $|\mathcal{H}^{(k)}|$ independent sub-problems. For each $\mathcal{H}_j^{(k)}\in\mathcal{H}^{(k)}$, the independent sub-problem is to solve the following equation:

\begin{equation}
(\bm{\Theta}^{(k)}_{H_j^{(k)}})^{-1}=\mathcal{S}_{\mathcal{H}_{j}^{(k)}}^{(k)} + \rho \times (\bm{\Theta}_{\mathcal{H}_{j}^{(k)}}^{(k)} - \bm{Y}_{\mathcal{H}_{j}^{(k)}}^{(k)} + \bm{U}_{\mathcal{H}_{j}^{(k)}}^{(k)})
\end{equation}

Solving Eq (10) requires eigen-decomposition of small submatrices, which shall be much faster than the eigen-decomposition of the original large matrices. Based upon non-uniform partition, updating $\bm{Y}$ given $\bm{\Theta}$ and $\bm{U}$ and updating $\bm{U}$ given $\bm{Y}$ and $\bm{\Theta}$ are also faster than corresponding part of previous ADMM algorithm for solving group graphical lasso [9], because more zero entries are pre-identified and are not involved in the updating.

\begin{figure}[t]
 \centering
% \begin{subfigure}[b]{0.23\textwidth}
 \includegraphics[width=0.23\textwidth]{img/fig/1.pdf}
% \subcaption{}
 %\end{subfigure}
% \begin{subfigure}[b]{0.23\textwidth}
 \includegraphics[width=0.23\textwidth]{img/fig/2.pdf}
% \subcaption{}
 %\end{subfigure}
 %\begin{subfigure}[b]{0.23\textwidth}
 \includegraphics[width=0.23\textwidth]{img/fig/3.pdf}
% \subcaption{}
 %\end{subfigure}
 %\begin{subfigure}[b]{0.23\textwidth}
 \includegraphics[width=0.23\textwidth]{img/fig/4.pdf}
% \subcaption{}
 %\end{subfigure}
\caption{The logarithm of the gap between the primal and dual variables with respect to the number of iterations. (a) On a two classes type C dataset  with $p=1000$, $\lambda_1=0.009$ and $\lambda_2=0.0005$. (b) On a four classes type C dataset with $p=1000$, $\lambda_1=0.0086$ and $\lambda_2=0.001$. (c) On a six classes type A dataset with $p=1000$, $\lambda_1=0.0082$ and $\lambda_2=0.0015$. (d) The objective function value with respect to the number of iterations on a two classes type C data with $p=1000$, $\lambda_1=0.009$ and $\lambda_2=0.0005$.}
\end{figure}

\section{Experimental Results}

In this section, we test our method, denoted as HADMM (i.e., hybrid screening algorithm + ADMM), using both synthetic and real datasets and also compare HADMM with two control methods: 1) GADMM: global screening algorithm + ADMM; and 2) LADMM: class-specific thresholding algorithm +ADMM.

We have implemented the three methods with C++ and R, and tested them on a Linux machine with Intel Xeon E5-2670 2.6GHz. We first show that HADMM can converge to the optimal solution obtained by the plain ADMM without any screening through experiments. Then we compare the running time of HADMM with GADMM and LADMM by calculating the consumed time at the same number of iterations.

To generate a dataset with $K$ classes from Gaussian distribution, we first randomly generate $K$ precision matrices and then use them to sample $5 \times p$ data points for each class. To make sure that the randomly-generated precision matrices are positive definite, we set all the diagonal entries to 5.0, and an off-diagonal entry to either 0 or $ \pm r \times 5.0$ . We generate three types of datasets as follows.
\begin{itemize}
\item\textbf{Type A}: 97\% of the entries in a precision matrix are 0.
\item	\textbf{Type B}: the $K$ precision matrices have same diagonal block structure.	
\item	\textbf{Type C}: the $K$ precision matrices have slightly different diagonal block structures.
\end{itemize}
For \textbf{Type A}, $r$ is set to be less than 0.0061. For \textbf{Type B} and \textbf{Type C}, $r$ is smaller than 0.0067. For each type we generate 18 datasets by setting $K=2,3,\ldots,10$, and $p=1000,\ 10000$, respectively.


\subsection{Correctness of Hybrid Algorithm on Synthetic Data}

\begin{figure*}[t]
 \centering
 \begin{subfigure}[b]
  \centering
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/A/1.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/A/2.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/A/3.pdf}
 \subcaption{\textbf{Type A}}
 \end{subfigure}
 \begin{subfigure}[b]
  \centering
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/B/1.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/B/2.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/B/3.pdf}
 \subcaption{\textbf{Type B}}
  \end{subfigure}
 \begin{subfigure}[b]
  \centering
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/C/1.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/C/2.pdf}
 \includegraphics[width=0.28\textwidth]{img/fig/fig4/C/3.pdf}
 \subcaption{\textbf{Type C}}
 \end{subfigure}
\caption{Logarithm of the running time (in seconds) of HADMM, LADMM and GADMM for $p = 1000$ on \textbf{Type A, Type B} and \textbf{Type C} data.}
\end{figure*}

To evaluate the correctness of HADMM, we compare HADMM with ADMM, GADMM and LADMM. We run all the methods for $500$ iterations over the tree types of data with $p=1000$. As shown in Figure 2(d) and Supplementary Figures 3-5, all the four methods converge to very close solutions. Note that we select several pairs of parameters ($\lambda_1$,$\lambda_2$) during our simulation. Please refer to the supplementary material for our model selection.

We also examined the gap between the primal and dual variables in the ADMM algorithms, which is defined as $|\bm{\Theta}-\bm{Y}|_1$. According to Figure $2(a)-(c)$, regardless of the data type, the number of classes and the parameters used, HADMM converges the fastest according to the primal-dual variable gap. In particular, after 200 iterations, although the other three methods (GADMM, LADMM and ADMM) can hardly reduce the primal-dual gap, our HADMM can still significantly reduce it. This is because our hybrid screening algorithm can effectively identify many more zero entries in the precision matrices and split the them into much smaller sub-matrices. See more results for verifying the correctness of HADMM in supplementary matereial.

\subsection{Performance on Synthetic Datasets}
We first compare the running time of the three methods using different parameters $\lambda_{1}$ and $\lambda_{2}$ over the three types of data. See supplementary material for model selection. We display the result for $p=1000$ in Figure 3 and that for $p=10000$ in supplementary material, respectively. In previous section we have shown that our HADMM converges at a similar rate as the other three ADMM methods before 200 iterations and much faster after 200 iterations. Here we test the running times of HADMM, LADMM and GADMM needed for 500 iterations, at which HADMM shall have a better accuracy than LADMM and GADMM.

In Figure 3, each row has the experimental results on one type of data (\textbf{Type A}, \textbf{Type B} and \textbf{Type C} from left to right). Each column has the experimental results for the same set of parameters ($\lambda_1 = 0.009$, $\lambda_2 = 0.0005$, $\lambda_1 = 0.0086$, $\lambda_2 = 0.001$ and $\lambda_1 = 0.0082$, $\lambda_2 = 0.0015$ from left to right). As shown in Figure 3(a)-(i), HADMM is much more efficient than LADMM. GADMM performs comparably to or better than LADMM when $\lambda_{2}$ is large. The running time of LADMM significantly increases as $\lambda_{1}$ decreases. Also, the running time of both LADMM and GADMM increases significantly as the number of classes increases. By contrast, HADMM is not very sensitive to the number of classes. Moreover, as our hybrid algorithm yields finer non-uniform feasible partitions, the precision matrices are more likely to be split into many more smaller submatrices. This means it is potentially easier to parallelize HADMM and achieve even more significant speedup.


We also compare the three screening algorithms in terms of the estimated computational complexity for matrix eigen-decomposition, a time-consuming subroutine used by the ADMM algorithms. Given a partition $\mathcal{H}$ of the variable set of $\mathcal{V}$, the computational complexity can be estimated by $\sum_{\mathcal{H}_{i} \in \mathcal{H}} |\mathcal{H}_{i}|^{3}$. As shown in Figure 9-17 in supplementary material, when $p=1000$, the non-uniform thresholding algorithm generates partitions with much smaller computational complexity, usually $\frac{1}{10} \sim \frac{1}{1000}$ of the other two methods. Note that the y-axis is the logarithm of the estimated computational complexity. When $p = 10000$, the gap between the non-uniform thresholding algorithm and the other two are even larger, as shown in Figure 33-41 in supplementary.

\subsection{Performance on Real Gene Expression Data}

\begin{figure}[t]
\centering
\includegraphics[width=0.85\columnwidth]{fig/fig5/2.jpg}
\caption{Network of the first 100 genes in class $\lambda_1=0.8$, $\lambda_2=0.2$.}
\end{figure}

We test our methods on a lung cancer data (accession number GDS2771) downloaded from Gene Expression Omnibus and a mouse immune dataset described in [24]. The lung cancer data is collected from 97 patients with lung cancer and 90 controls without lung cancer, so this dataset consists of two different classes: patient and control. The immune dataset consists of 214 cell types, which can be clustered into three classes. These three classes share 10555 common genes, so this dataset has 10555 features and 3 classes. The running time of the three algorithms are shown in Table 1 and the resultant gene networks with different sparsity are shown in supplementary. As shown in Table 1 (\textbf{Setting 1}: $\lambda_{1} = 0.8, \lambda_{2} = 0.2$; \textbf{Setting 2}: $\lambda_{1} = 0.9, \lambda_{2} = 0.15$ and \textbf{Setting 3}: $\lambda_{1} = 0.95, \lambda_{2} = 0.1$), our hybrid screening algorithm is much more efficient than the other two screening methods. An example gene network is shown in Figure $4$ others are shown in supplementary materials.


\begin{table}[h]
\caption{Running time (hours) of HADMM, LADMM and GADMM on real data}
\begin{center}
	\begin{tabular}{l | c | c | c}
	{\bf Method} & {\bf Setting 1} &{\bf Setting 2} &{\bf Setting 3}	\\
	\hline
	HADMM 	& 13.3 & 9.6 & 6.4	  \\
	LADMM 	& $\geq$ 144 & $\sim$ 120 & $\sim$ 96 	\\
	GADMM 	& $\sim$ 119 & $\geq$ 144 & $\geq$ 144 \\
	\end{tabular}
\end{center}
\end{table}


% In the unusual situation where you want a paper to appear in the
% references without citing it in the main text, use \nocite
\nocite{langley00}

\bibliography{example_paper}
\bibliographystyle{icml2015}

\end{document} 


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was
% created by Lise Getoor and Tobias Scheffer, it was slightly modified  
% from the 2010 version by Thorsten Joachims & Johannes Fuernkranz, 
% slightly modified from the 2009 version by Kiri Wagstaff and 
% Sam Roweis's 2008 version, which is slightly modified from 
% Prasad Tadepalli's 2007 version which is a lightly 
% changed version of the previous year's version by Andrew Moore, 
% which was in turn edited from those of Kristian Kersting and 
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.  
